# CertiCAV
# Copyright (C)2021 Connected Places Catapult
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact: musicc-support@cp.catapult.org.uk

## @package TestScripts
#
# This package is to encapsulate the test scripts (which are used to analyse the performance of the ADS/Human in the scenarios)
import math
import time
import traceback
import json
import os
from enum import Enum

## Class used to encode the python object to JSON String
class ComplexEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj,'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)


## Class to encapsulate the test results of all test on all scenarios
class FullTestsReport:
    def __init__(self):
        self.name = ""
        self.testTypes = []
        self.resultDict = {
            "Unknown" : 0,
            "Pass" : 1,
            "Human Trials Required" : 2,
            "Non-Compliant" : 3
        }
        
    ## Adds an instance of the object TestType to the current FullTestsReport
    def addTestResult(self,testResults):
        testTypeID = -1
        for testType in testResults.testTypes:
            for itr in range(0,len(self.testTypes)):
                if self.testTypes[itr].name == testType.name:
                    testTypeID = itr
                    break
            if testTypeID == -1:
                self.testTypes.append(FullTestsReport.TestType())
                testTypeID = len(self.testTypes) - 1
                self.testTypes[testTypeID].name = testResults.testTypes[0].name
            
            for test in testType.tests:
                self.testTypes[testTypeID].addTest(test)

    ## Fills the result field dependant on its lower objects results
    def fillResultFields(self):
        for testType in self.testTypes:
            testType.fillResultFields(self.resultDict)
    
    ## Used in the encoding of the object
    def reprJSON(self):
        return dict(name=self.name, testTypes=self.testTypes)
        
    ## Class to encapsulate the test types of all test on all scenarios
    class TestType:
        def __init__(self):
            self.name = ""
            self.result = "Unknown"
            self.tests = []

        ## Adds an instance of the object Test to the current TestType
        def addTest(self,test):
            testID = -1
            for itr in range(0,len(self.tests)):
                if self.tests[itr].name == test.name:
                    testID = itr
                    break
            if testID == -1:
                self.tests.append(FullTestsReport.TestType.Test())
                testID = len(self.tests) - 1
                self.tests[testID].name = test.name
            for groupedScenario in test.groupingScenarios:
                self.tests[testID].addGroupedScenario(groupedScenario)

        ## Fills the result field dependant on its lower objects results
        def fillResultFields(self, resultDict):
            for test in self.tests:
                test.fillResultFields(resultDict)
                if resultDict[test.result] > resultDict[self.result]:
                            self.result = test.result

        ## Used in the encoding of the object
        def reprJSON(self):
            return dict(name=self.name,result=self.result, tests=self.tests) 
        
        ## Class to encapsulate the tests on all scenarios
        class Test:
            def __init__(self):
                self.name = ""
                self.result = "Unknown"
                self.groupingScenarios = []

            ## Adds an instance of the object GroupingScenario to the current FullTestsReport
            def addGroupedScenario(self,groupedScenario):
                groupedScenarioID = -1
                for itr in range(0,len(self.groupingScenarios)):
                    if self.groupingScenarios[itr].name == groupedScenario.name:
                        groupedScenarioID = itr
                        break
                if groupedScenarioID == -1:
                    self.groupingScenarios.append(FullTestsReport.TestType.Test.GroupingScenario())
                    groupedScenarioID = len(self.groupingScenarios) - 1
                    self.groupingScenarios[groupedScenarioID].name = groupedScenario.name
                for scenario in groupedScenario.scenarios:
                    self.groupingScenarios[groupedScenarioID].addScenario(scenario)

            ## Fills the result field dependant on its lower objects results
            def fillResultFields(self, resultDict):
                for groupedScenario in self.groupingScenarios:
                    groupedScenario.fillResultFields(resultDict)
                    if resultDict[groupedScenario.result] > resultDict[self.result]:
                            self.result = groupedScenario.result

            ## Used in the encoding of the object
            def reprJSON(self):
                return dict(name=self.name, result=self.result, groupingScenarios=self.groupingScenarios) 

            ## Class to encapsulate the grouping scenario on all scenarios
            class GroupingScenario:
                def __init__(self):
                    self.name = ""
                    self.result = "Unknown"
                    self.scenarios = []

                ## Adds an instance of the object Scenario to the current GroupingScenario
                def addScenario(self,scenario):
                    scenarioID = -1
                    for itr in range(0,len(self.scenarios)):
                        if self.scenarios[itr].name == scenario.name:
                            scenarioID = itr
                            print("Test results have already been allocated for this scenario")
                    if scenarioID == -1:
                        self.scenarios.append(FullTestsReport.TestType().Test.GroupingScenario.Scenario())
                        scenarioID = len(self.scenarios) - 1
                        self.scenarios[scenarioID].id = scenarioID
                        self.scenarios[scenarioID].name = scenario.name
                        self.scenarios[scenarioID].result = scenario.result
                        self.scenarios[scenarioID].score = scenario.score
                        self.scenarios[scenarioID].message = scenario.message
                    return None
                
                ## Fills the result field dependant on its lower objects results
                def fillResultFields(self,resultDict):
                    for scenario in self.scenarios:
                        if resultDict[scenario.result] > resultDict[self.result]:
                            self.result = scenario.result

                ## Used in the encoding of the object
                def reprJSON(self):
                    return dict(name=self.name, result=self.result, scenarios=self.scenarios) 

                ## Class to encapsulate a scenario
                class Scenario:
                    def __init__(self):
                        self.name = ""
                        self.result = "Unknown"
                        self.score = "N/A"
                        self.message = ""
                    
                    ## Used in the encoding of the object
                    def reprJSON(self):
                        return dict(name=self.name, result=self.result, score=self.score, message=self.message) 

## This method is used to run all of the tests on all of the scenarios passed into it and from those results, create a test results file 
#
# @param scenarios The List compiling of each CertiTRACE output from the scenarios executed
# @param queryResultDirectory The output directory from this run of CertiCAV-Master.py
# @param human Boolean value determining if the scenario outputs were from a human or ADS (different TestResult output filenames)
def RunTests(scenarios, queryResultDirectory, human = False):
    try:    
        fullTestsReport = FullTestsReport()
        fullTestsReport.name = "Test Report"
        for scenario in scenarios:
            #TODO: To add test : Write Test Method - > fullTestsReport.addTestResult({YourTest(scenario)})
            print("Not Tests implemented Yet")

        fullTestsReport.fillResultFields()
        if human:
            humanItr = 1
            while os.path.exists(queryResultDirectory +  "/" + "TestResults(Human_{0}).txt".format(humanItr)):
                humanItr += 1
            fullTestReportFile = open(queryResultDirectory +  "/" + "TestResults(Human_{0}).txt".format(humanItr), "w")
        else:
            fullTestReportFile = open(queryResultDirectory +  "/" + "TestResults.txt", "w")
        fullTestReportFile.write(json.dumps(fullTestsReport.reprJSON(), cls=ComplexEncoder))
        fullTestReportFile.close()
        
        return fullTestsReport
    except Exception as e:
        traceback.print_exc()

## This method initialises an instance of FullTestsReport for one of the tests to fill in.
#
# Initialises an instance of FullTestsReport with test type name, test name and scenario name filled in, for use in populating the results from a singular test that is then added back into the collective FullTestsReport using the method FullTestsReport.addTestResult()
#
# @param testTypeName The test type name of the test that this FullTestsReport instance is going to be filled in by
# @param testName The name of the test that this FullTestsReport instance is going to be filled in by
# @param scenario The CertiTRACE output from one of the scenarios
def InitialiseTestResultVariable(testTypeName,testName,scenario):
    test = FullTestsReport()
    test.testTypes.append(FullTestsReport.TestType())
    test.testTypes[0].tests.append(FullTestsReport.TestType.Test())
    test.testTypes[0].tests[0].groupingScenarios.append(FullTestsReport.TestType.Test.GroupingScenario())
    test.testTypes[0].tests[0].groupingScenarios[0].scenarios.append(FullTestsReport.TestType.Test.GroupingScenario.Scenario())
    
    test.testTypes[0].name = testTypeName
    test.testTypes[0].tests[0].name = testName
    test.testTypes[0].tests[0].groupingScenarios[0].name = scenario.groupingScenarioIdentifier
    test.testTypes[0].tests[0].groupingScenarios[0].scenarios[0].name = scenario.concreteScenarioIdentifier

    return test



