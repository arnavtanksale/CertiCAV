# CertiCAV
# Copyright (C)2021 Connected Places Catapult
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact: musicc-support@cp.catapult.org.uk

## @package OSCToHumanTrial
#
# This file is part of the Example Implementation and used to convert the OSC to allow the scenario runner to ignore osc control for the ego and use an external source
import xml.etree.ElementTree as ET
import copy

## This method reads in the OSC from the passed in path and inserts external control for the ego vehicle to allow for human control 
def ConvertOSCToHumanTrialOSC(path):
    tree = ET.parse(path)
    root = tree.getroot()

    humanControlTree = ET.parse("./Utility-Scripts/HumanController.xml")
    humanControlRoot = humanControlTree.getroot()

    # Inversing relative lane change values / ensuring lane change dynamicsShape is linear / ensuring lane change dynamicsDimension is distance / ensuring routeStrategy is always fastest 
    for storyboard in root.findall("Storyboard"):
        for init in storyboard.findall("Init"):
            for action in init.findall("Actions"):
                for private in action.findall("Private"):
                    entityRef = private.get("entityRef")
                    if entityRef != None:
                        if entityRef.lower() == "ego" or entityRef.lower() == "hero":
                            private.append(copy.deepcopy(humanControlRoot))
                            print("Found Ego Init, inserting Controller")
    tree.write(path[0:-5] + "_HumanTrial.xosc")


if __name__ == "__main__":
    Convert_OSC_To_Human_Trial_OSC("/home/samnichols/OSC-ALKS-scenarios-master/Scenarios/ALKS_Scenario_4.4_2_CutInUnavoidableCollision_TEMPLATE.xosc")
