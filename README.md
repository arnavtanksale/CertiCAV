# Project Overview

CertiCAV was a project which aimed to identify principles and generate evidence relating to the type approval and safety assurance of Highly Automated Vehicles (HAVs). The project only considered the safety of the perception and tactical decision making of a nominally performing HAV (i.e. it does not attempt to address operational safety or component failures).
 
This repository contains source code for a software framework developed as part of the CertiCAV project. It represents a proof-of-concept implementation of automated testing methods for HAV assessment, applied in a way which is compatible with the wider CertiCAV framework (which is currently unpublished). Developing it was intended to generate knowledge about how to practically apply this type of testing, to inform both the rest of the CertiCAV project and any future work based on its outputs.
 
The software was developed by Connected Places Catapult. A [report describing its architecture and learnings](https://cp.catapult.org.uk/news/implementing-and-evaluating-safety-metrics-for-cavs-report/) for HAV verification is available.

# Example Implementation

This repository includes the core part of the CertiCAV Software framework, along with MUSICC as an example implementation for the Scenario Database. The core framework allows the developer to integrate a simulator into this framework by writing code into the empty methods "InitialiseSimulator" and "Run Scenarios". This release also includes code to interface with MUSICC, but the developer could replace this with another scenario database.

We have implemented a full example implementation with the following applications:

- Scenario Database - MUSICC 

- Simulator - CARLA 

- Scenario Runner - scenario_runner

To find this example implementation, go to : https://gitlab.com/connected-places-catapult/CertiCAV-ExampleImplementation

# Quick Start Guide

## Python

1. Install Python 3.7 (https://www.python.org/downloads/)

2. If you have multiple versions of Python on your machine ensure version 3.7 is the one you are using (this can be done using system paths, virtual environments, calling the directory...etc).

3. Open terminal and navigate to the repo's directory (/CertiCav/).

4. Ensure you are using python 3.7 by running `python --version`

5. Install the dependencies by running `pip install -r requirements`

## CertiTRACE

CertiTRACE is a protobuf trace format made up of a proto file that references a slightly altered version of the ASAM OSI proto files (https://github.com/OpenSimulationInterface).

These are already compiled and included in the Repository but if you wish to rebuild follow the instructions below:

To compile these proto files you need to :

1. Open terminal and change directory to the repo's directory (/CertiCAV/).

2. Run the following command in the terminal `./Compile_Protos.sh` 

## MUSICC  (The scenario database used for the example implementation)

1. Go to https://musicc.ts-catapult.org.uk/accounts/login/

2. Click "Sign up".

3. Fill in the details on the page and click "Sign up".

4. Once you have an account you need to fill in those MUSICC details into the Config.json data in the fields (ScenarioDatabaseCredentials -> Username and Password).

## Windows Notes

The software should work exactly the same on Microsoft Windows, with two notes:
1. Once CARLA is installed, ensure you set the PYTHONPATH environment variable as documented in the [Scenario Runner quick-start](https://carla-scenariorunner.readthedocs.io/en/latest/getting_started/).
2. A binary version of the Geos library is needed for the Python dependency Shapely. The simplest way of getting this is to install Shapely 1.7.1 from https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely (and updating the requirements.txt for this repo to have Shapely==1.7.1)

# How To Run

Once you have everything installed, open up the config.json file and fill in the fields to configure the program to your liking.
At this point you're ready to run CertiCAV:
1. Open up CMD/Terminal in the CertiCAV folder
2. Run "Python CertiCAV-Master.py"
That's it! You're now running CertiCAV and can navigate yourself through the command line menus to perform your required tasks

# Documentation

The documentation for the project is compiled of a combination of in-code comments and auto generated doxygen documentation found in the repo under /CertiCAV/Documentation/ 

The Main Page being found at /CertiCAV/Documentation/html/index.html

# Useful Links

MUSICC (Scenario Database): https://cp.catapult.org.uk/project/multi-user-scenario-catalogue-for-connected-and-autonomous-vehicles/

OpenSCENARIO (Scenario Descriptor): https://www.asam.net/standards/detail/openscenario/

OpenDRIVE (Scenario Road Network Descriptor): https://www.asam.net/standards/detail/opendrive/

OSI (Base of CertiTRACE, i.e ground truth): https://www.asam.net/standards/detail/osi/

# Who do I talk to? 

Support -  musicc-support@cp.catapult.org.uk

Repo Owner - Sam Nichols (Sam.Nichols@cp.catapult.org.uk)

Moderator - Robert Myers (Robert.Myers@cp.catapult.org.uk)

Moderator - Zeyn Saigol (Zeyn.Saigol@cp.catapult.org.uk)

